package pl.kck.runner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EntityScan(basePackages = {"pl.kck.domain.model"})
@ComponentScan(
        basePackages = {
                "pl.kck.users.controllers",
                "pl.kck.mpc.controllers",
                "pl.kck.users.repository",
                "pl.kck.mpc.repository",
                "pl.kck.users.services",
                "pl.kck.mpc.services",
                "pl.kck.security.config"
        })
@SpringBootApplication(scanBasePackages = {"pl.kck"})
//@EnableAutoConfiguration(exclude = {SecurityAutoConfiguration.class})
@EnableJpaRepositories(
        basePackageClasses = {
                pl.kck.users.repository.UsersRepository.class,
                pl.kck.mpc.repository.MpcRepository.class,
                pl.kck.mpc.repository.MpcUsersRepository.class,
        })
public class KckApplication {

    public static void main(String[] args) {
        SpringApplication.run(KckApplication.class, args);
    }
}

