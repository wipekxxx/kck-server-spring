package pl.kck.domain.model;

import lombok.*;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class MpcRequest implements Serializable {
    private static final long serialVersionUID = 1L;

    private Mpc mpc;

    private String userId;
}
