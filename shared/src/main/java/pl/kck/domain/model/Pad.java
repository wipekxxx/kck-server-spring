package pl.kck.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Data
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
@EqualsAndHashCode(callSuper = true, exclude = {"mpc"})
@ToString(exclude = {"mpc"})
@Table(name = "Pads")
public class Pad extends BaseEntity<String> {
    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Column
    private String padChar;

    @Column
    private String padText;

    @Column
    @Type(type = "text")
    private String base64encodedFile;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MPC_ID")
    private Mpc mpc;
}
