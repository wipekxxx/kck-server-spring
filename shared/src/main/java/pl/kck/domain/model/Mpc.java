package pl.kck.domain.model;

import lombok.*;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
@EqualsAndHashCode(callSuper = true, exclude = {"tags", "user", "pads"})
@ToString(exclude = {"tags", "user", "pads"})
@Table(name = "Mpcs")
public class Mpc extends BaseEntity<String> {
    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Column
    private String name;

    @Column
    private String describe;

    @Column
    private String author;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
            name = "Mpc_Tags",
            joinColumns = {@JoinColumn(name = "MPC_ID")},
            inverseJoinColumns = {@JoinColumn(name = "TAG_ID")}
    )
    private Set<Tag> tags = new HashSet<>();

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "USER_ID")
    private User user;

    @OneToMany(mappedBy = "mpc", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Pad> pads = new HashSet<>();


}
