package pl.kck.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javafx.beans.property.StringProperty;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@Data
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
@EqualsAndHashCode(callSuper = true, exclude = {"mpcs"})
@ToString(exclude = {"mpcs"})
@Table(name = "Users")
public class User extends BaseEntity<String> {
    private static final long serialVersionUID = 1L;

//    @GeneratedValue(generator = "uuid2")
//    @GenericGenerator(name = "uuid2", strategy = "uuid2")
//    @Column(columnDefinition = "BINARY(16)")
    @Id
    private String id;

    @Column
    private String username = "";

    @Column
    private String email;

    @JsonIgnore
    @Column
    private String password;

    @JsonIgnore
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    private Set<Mpc> mpcs;
}
