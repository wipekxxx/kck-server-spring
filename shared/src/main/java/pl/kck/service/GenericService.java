package pl.kck.service;

import java.util.Collection;
import java.util.UUID;

public interface GenericService<T> {

    public Collection<T> findAll();
    public T findOne(Object id);
    public T save(T object);
    public void delete(T object);
}
