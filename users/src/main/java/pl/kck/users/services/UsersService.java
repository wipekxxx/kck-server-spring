package pl.kck.users.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.kck.domain.model.User;
import pl.kck.service.GenericService;
import pl.kck.users.repository.UsersRepository;

import java.util.Collection;

@Service
public class UsersService implements GenericService<User> {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UsersRepository repository;

    @Override
    public Collection<User> findAll() {
        return (Collection<User>) repository.findAll();
    }

    @Override
    public User findOne(Object id) {
        return repository.findOne((String) id);
    }

    @Override
    public User save(User object) {
        object.setPassword(passwordEncoder.encode("12345"));
        return repository.save(object);
    }

    @Override
    public void delete(User object) {
        if (repository.exists(object.getId())) {
            repository.delete(object);
        }
    }
}
