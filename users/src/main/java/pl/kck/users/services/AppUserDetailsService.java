package pl.kck.users.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import pl.kck.domain.model.User;
import pl.kck.users.repository.UsersRepository;

import java.util.ArrayList;
import java.util.List;

//@Service(value = "userDetailsService")
@Component
public class AppUserDetailsService implements UserDetailsService {

    public final static String DEFAULT_ROLE = "user";

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = usersRepository.findByUsername(s);

        if(user == null) {
            throw new UsernameNotFoundException(String.format("The username %s doesn't exist", s));
        }

        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(DEFAULT_ROLE));


        return new org.springframework.security.core.userdetails.
                User(user.getUsername(), "12345", authorities);
    }
}
