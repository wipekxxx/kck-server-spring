package pl.kck.users.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import pl.kck.domain.model.User;
import pl.kck.users.services.UsersService;

import javax.validation.Valid;
import java.util.UUID;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@CrossOrigin
@RestController
@RequestMapping("/api/users")
public class UsersController {

    @Autowired
    private UsersService service;

    @RequestMapping(method = POST, value = "/register")
    public ResponseEntity register(@RequestBody User user) {
        if (service.findOne(user.getId()) == null) {
            service.save(user);
        }
        return new ResponseEntity(service.findOne(user.getId()), HttpStatus.OK);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(method = GET)
    public ResponseEntity getAll() {
        return new ResponseEntity(service.findAll(), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(method = GET, value = "/{id}")
    public ResponseEntity getUser(@PathVariable UUID id) {
        User user = service.findOne(id);
        return (user != null) ? new ResponseEntity(user, HttpStatus.OK) : new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @SuppressWarnings("unchecked")
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(method = {POST, PUT, OPTIONS})
    public ResponseEntity addUpdateUser(@Valid @RequestBody User user) {
        service.save(user);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(method = DELETE, value = "/{id}")
    public ResponseEntity deleteUser(@PathVariable UUID id) {
        User user = service.findOne(id);
        if (user != null) {
            service.delete(user);
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(HttpStatus.OK);
    }
}
