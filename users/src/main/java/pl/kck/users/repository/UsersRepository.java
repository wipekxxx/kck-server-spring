package pl.kck.users.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import pl.kck.domain.model.User;

import java.util.UUID;

public interface
UsersRepository extends CrudRepository<User, String> {

    @Query(nativeQuery = true, value = "SELECT * FROM USERS WHERE USERNAME = :username LIMIT 1")
    User findByUsername(@Param("username") String username);
}
