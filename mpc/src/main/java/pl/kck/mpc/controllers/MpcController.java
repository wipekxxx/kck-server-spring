package pl.kck.mpc.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.kck.domain.model.Mpc;
import pl.kck.domain.model.MpcRequest;
import pl.kck.mpc.repository.MpcRepository;
import pl.kck.mpc.services.MpcService;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@CrossOrigin
@RestController
@RequestMapping("/api/mpc")
public class MpcController {

    @Autowired
    private MpcRepository repository;

    @Autowired
    private MpcService service;

    @RequestMapping(method = GET)
    public ResponseEntity getAll() {
        Set<Mpc> mpcs = new HashSet<Mpc>((Collection<? extends Mpc>) repository.findAll());
        mpcs.forEach(mpc -> mpc.setPads(null));
        return mpcs.size() > 0 ? new ResponseEntity(mpcs, HttpStatus.OK) : new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(method = POST)
    public ResponseEntity saveMpc(@RequestBody MpcRequest mpcRequest) {
        service.save(mpcRequest);
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }

    @RequestMapping(method = POST, value = "/update")
    public ResponseEntity updateMpc(@RequestBody MpcRequest mpcRequest) {
        service.update(mpcRequest);
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }

    @RequestMapping(method = GET, value = "/{id}")
    public ResponseEntity getMpc(@PathVariable String id) {
        Mpc mpc = repository.findOne(id);
        return (mpc != null) ? new ResponseEntity(mpc, HttpStatus.OK) : new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(method = DELETE, value = "/{id}")
    public ResponseEntity removeMpc(@PathVariable String id) {
        repository.delete(id);
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }
}
