package pl.kck.mpc.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kck.domain.model.Mpc;
import pl.kck.domain.model.MpcRequest;
import pl.kck.domain.model.Tag;
import pl.kck.mpc.repository.MpcRepository;
import pl.kck.mpc.repository.MpcUsersRepository;
import pl.kck.mpc.repository.TagsRepository;

import java.util.UUID;

@Service
public class MpcService {

    @Autowired
    private MpcRepository repository;

    @Autowired
    private MpcUsersRepository usersRepository;

    @Autowired
    private TagsRepository tagsRepository;

    public void save(MpcRequest mpcRequest) {
        Mpc mpc = mpcRequest.getMpc();
        mpc.getPads().forEach((pad) -> pad.setMpc(mpc));
        mpc.getTags().forEach(tag -> {
            Tag tagFromDb = tagsRepository.findTagByName(tag.getName());
            if (tagFromDb != null) {
                tag.setId(tagFromDb.getId());
            } else {
                tag.setId(UUID.randomUUID());
            }
        });
        mpc.setUser(usersRepository.findOne(mpcRequest.getUserId()));
        repository.save(mpc);
    }

    public void update(MpcRequest mpcRequest) {
        Mpc mpc = mpcRequest.getMpc();
        mpc.getPads().forEach((pad) -> pad.setMpc(mpc));
        mpc.getTags().forEach(tag -> {
            Tag tagFromDb = tagsRepository.findTagByName(tag.getName());
            if (tagFromDb != null) {
                tag.setId(tagFromDb.getId());
            } else {
                tag.setId(UUID.randomUUID());
            }
        });
        mpc.setUser(usersRepository.findOne(mpcRequest.getUserId()));
        repository.save(mpc);
    }
}
