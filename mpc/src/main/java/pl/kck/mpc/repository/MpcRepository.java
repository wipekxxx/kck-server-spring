package pl.kck.mpc.repository;

import org.springframework.data.repository.CrudRepository;
import pl.kck.domain.model.Mpc;

public interface MpcRepository extends CrudRepository<Mpc, String> {


}
