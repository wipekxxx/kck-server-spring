package pl.kck.mpc.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import pl.kck.domain.model.User;

public interface
MpcUsersRepository extends CrudRepository<User, String> {

}
