package pl.kck.mpc.repository;

import org.springframework.data.repository.CrudRepository;
import pl.kck.domain.model.Tag;

import java.util.UUID;

public interface TagsRepository extends CrudRepository<Tag, UUID> {

    Tag findTagByName(String name);
}
